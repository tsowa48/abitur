<%@page contentType="text/html" pageEncoding="UTF-8"%><!DOCTYPE html>
<html>
  <head>
    <%@include file="/WEB-INF/header.jspf" %>
    <title>Абитуриент</title>
  </head>
  <body>
    <div class='container'>
      <div class='row text-center'>
        <a class='btn btn-lg btn-success' href='enroll'>Я хочу поступить!</a>
      </div>
    </div>
  </body>
</html>
